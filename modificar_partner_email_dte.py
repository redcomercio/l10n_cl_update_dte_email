# -- coding: utf-8 --
#************#
# Importación Clientes y Proveedores #
#************#

import os
import csv
import xmlrpclib
import re

"""
HOST='34.195.205.174'
PORT=8069
DB='db10-chile-sii'
USER='admin'
PASS='myorsan2017'
path_file = 'empresas.csv'
url ='http://%s:%d/xmlrpc/' % (HOST,PORT)
"""

HOST='34.195.205.174'
PORT=8069
DB='redcomercio'
USER='admin'
PASS='myorsan2017'
path_file = 'empresas_1.csv'
url ='http://%s:%d/xmlrpc/' % (HOST,PORT)


common_proxy = xmlrpclib.ServerProxy(url+'common')
object_proxy = xmlrpclib.ServerProxy(url+'object')
uid = common_proxy.login(DB,USER,PASS)

def _update_mass(path_file):
    archive = csv.DictReader(open(path_file),delimiter=';')
    cont = 0
    for field in archive:
        cont = cont + 1
        rut = field['RUT']
        #int_rut = rut.replace(".", '').replace("-", '')
        int_rut =  int_rut[:-8] + '.' +int_rut[-8:-5] + '.' +int_rut[-5:-2] + int_rut[-2:]

        # Buscamos el partner con el nombre
        partner = object_proxy.execute(DB,uid,PASS,'res.partner','search',[('document_number','=',int_rut)])
        partner_id = partner and partner[0]
        if partner_id:
            vals = {}

             # Campos Nuevos
            vals['dte_email'] = field['MAIL INTERCAMBIO']

            # Escribimos en el Modelo
            do_write = object_proxy.execute(DB,uid,PASS,'res.partner', 'write', partner_id, vals)


            print str(cont) + '---> ' + rut

def __main__():
    print 'Ha comenzado el proceso'
    _update_mass(True)
    print 'Ha finalizado la carga tabla'
__main__()

Ejemplo para usar Screen

Crerar nueva screen
$ screen -S update_dte

Salir de la Screen
$ Ctrl+a d

Volver a la Screen creada
$ screen -x update_dte

Cerrar la Screen
$ screen -S update_dte -X quit

Dividir archivos y cargar
python -c "import update_email; update_email.split_and_update('ce_empresas_dwnld_20180502.csv')"

Actaulizar correos de intercambio desde un archivo
python -c "import update_email; update_email.update_mass('ce_empresas_dwnld_20180502.csv')"

Actaulizar correos de intercambio desde dos archivos separados con csv splitter
python -c "import update_email; update_email.update_mass('empresas_1.csv'); _update_mass('empresas_2.csv')"

Para separar los csv
$ python -c "import csv_splitter;csv_splitter.split(open('ce_empresas_dwnld_20180502.csv', 'r'))"

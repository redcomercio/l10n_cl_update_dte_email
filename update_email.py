# -- coding: utf-8 --
#************#
# Importación Clientes y Proveedores #
#************#

import os
import csv
import xmlrpclib
import re
import time

HOST='34.195.205.174'
PORT=8069
DB='redcomercio'
USER='admin'
PASS='myorsan2017'
url ='http://%s:%d/xmlrpc/' % (HOST,PORT)
#filehandler = 'empresas_1.csv'

common_proxy = xmlrpclib.ServerProxy(url+'common')
object_proxy = xmlrpclib.ServerProxy(url+'object')
uid = common_proxy.login(DB,USER,PASS)

# Ejemplo
# screen -S update_dte
# Ctrl+a d
# screen -x update_dte
# screen -S update_dte -X quit
# python -c "import update_email; update_email.update_mass('ce_empresas_dwnld_20180502.csv')"
# python -c "import update_email; update_email.update_mass('empresas_1.csv'); _update_mass('empresas_2.csv')"

def update_mass(filehandler = 'ce_empresas_dwnld_20180502.csv'):
    succes_count = 0
    error_count = 0
    archive = csv.reader(open(filehandler),delimiter=';', quoting=csv.QUOTE_NONE)
    cont = 0
    for field in archive:
        cont = cont + 1
        rut = field[0]
        int_rut = rut.replace(".", '').replace("-", '')
        int_rut =  int_rut[:-7] + '.' +int_rut[-7:-4] + '.' +int_rut[-4:-1]+ '-' + int_rut[-1:]
        try:
            # Buscamos el partner con el nombre
            partner = object_proxy.execute(DB,uid,PASS,'res.partner','search',[('document_number','=',int_rut)])
            partner_id = partner and partner[0]
            if partner_id:
                vals = {}
                 # Campos Nuevos
                vals['dte_email'] = field[4]
                # Escribimos en el Modelo
                do_write = object_proxy.execute(DB,uid,PASS,'res.partner', 'write', partner_id, vals)
                print str(cont) + '---> ' + int_rut
                succes_count += 1
        except Exception as e:
            print 'This is an error message!' + int_rut
            error_count += 1

def __main__():
    print 'Ha comenzado el proceso'
    start_time = time.clock()
    update_mass()
    elapsed = time.time() - start_time
    hours, rem = divmod(elapsed, 3600)
    minutes, seconds = divmod(rem, 60)
    print 'Ha finalizado la carga tabla en: '
    print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
    print 'Registros cargados exitosamente: '+succes_count
    print 'Registros con error            : '+error_count
__main__()
